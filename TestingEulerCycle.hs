{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}

import Test.QuickCheck

import LabeledGraph
import Euler
import Graph

import Data.List
import Data.Maybe
import qualified Data.Set as Set

type TestInputCycle = [(String, String, String)]

-- tsykli koostamise abimeetod
makeCycle' :: [(a, a)] -> [(a, a, a)]
makeCycle' [] = []
makeCycle' (_:[]) = []
makeCycle' (x:y:zs) = (first, next, label) : (makeCycle' (y:zs))
    where   (first, label) = x
            (next, _) = y

-- tsykli koostamine tippude ja serva nimetuste paaridest
makeCycle :: [(a, a)] -> [(a, a, a)] 
makeCycle [] = []
makeCycle ((x, y) : []) = [(x, x, y)]
makeCycle (x : xs) = (lastV, firstV, lastLable) : (makeCycle' (x : xs))
    where   (firstV,_) = x
            (lastV, lastLable) = last xs

type TestGraph a = LabeledGraph a a

-- testimiseks koostatud andmetyyp,
-- mis annab pseudojuhuslikud graafid
instance (Arbitrary a, Ord a) => Arbitrary (TestGraph a) where
    arbitrary = do
        xs <- arbitrary
        return $ fromList $ makeCycle xs

-- graaf on iseendaga võrdne
propEquality :: TestGraph String -> Bool
propEquality g = g == g

-- kõik servad on tsüklis olemas
propAllEdgesInCycle :: TestGraph String -> Property
propAllEdgesInCycle g = isJust cycle ==> 
                            all (\e -> elem e $ fromJust cycle) (edges g)
    where cycle = eulerCycle g

-- kõik tsükli elemendid on graafi servad
propAllCycleElemsEdges :: TestGraph String -> Property
propAllCycleElemsEdges g = isJust cycle ==> 
                            all (\e -> elem e $ (edges g)) $ fromJust cycle
    where cycle = eulerCycle g

-- kõik pathi servad on unikaalsed (ükski serv ei kordu)
propAllEdgesInCycleUnique :: TestGraph String -> Property
propAllEdgesInCycleUnique g = isJust maybeCycle ==> cycle == nub cycle
    where   maybeCycle = eulerCycle g
            cycle = fromJust maybeCycle

-- kõik pathi servad on järjest
-- see tähendab, et iga järgneva serva algustipp on eelneva lõpp
propCycleSuccessive :: TestGraph String -> Property
propCycleSuccessive g = isJust cycle ==> successive $ fromJust cycle
    where   cycle = eulerCycle g
            successive [] = True
            successive (_ : []) = True
            successive (x : y : zs)
                | (finish x) /= (start y) = False
                | otherwise = successive $ y : zs