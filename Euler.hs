module Euler (
    eulerCycle
    ) where

import Graph
import qualified Data.Set as Set


-- kõikidel tippudel sisenevate ja väljuvate servade arv võrdne - tsükkel olemas
edgesCondition :: (Graph a b g) => g -> Bool
edgesCondition g =
    all (\x -> edgesCondition' (nextEdges x g) (previousEdges x g)) $ vertices g
    where   edgesCondition' Nothing _ = False
            edgesCondition' _ Nothing = False
            edgesCondition' (Just xs) (Just ys) = length xs == length ys

-- sügavuti tippude läbimine
visitVertex ::
    (Graph a b g, Ord a) => Set.Set a -> a -> g -> Set.Set a
visitVertex acc current g
    | Set.member current acc = acc
    | otherwise =
        visitEdges
        (Set.insert current acc)
        (nextEdges current g)
        g
    where   visitEdges acc' Nothing _ = acc'
            visitEdges acc' (Just es) g' =
                foldl
                (\acc'' e -> visitVertex acc'' (finish e) g')
                acc'
                es

-- kas leidub tee kõikidesse graafi tippudesse antud tipust
allOthersAccessible :: (Graph a b g, Ord a) => a -> g -> Bool
allOthersAccessible v g = allVisited (vertices g) $ visitVertex Set.empty v g
    where allVisited vs set = all (\x -> Set.member x set) vs

-- graaf on tugevalt sidus
connected :: (Graph a b g, Ord a) => g -> Bool
connected g
    | Graph.null g = True
    | otherwise = allOthersAccessible first g && allOthersAccessible first tg
    where   first = head $ vertices g
            tg = transpose g

-- Euleri tee leidmise abimeetod
eulerPath'' :: (Graph a b g, Ord a) => [b] -> [b] -> a -> g -> [b]
eulerPath'' acc [] _ _ = acc
eulerPath'' acc (e:es) current g
    | noIncomingEdges = eulerPath' (e:acc) next (removeVertex current newGraph)
    | allOthersAccessible next newGraph = eulerPath' (e:acc) next newGraph
    | otherwise = eulerPath'' acc es current g
    where   newGraph = removeEdge e g
            next = finish e
            noIncomingEdges = maybe
                                False
                                (\xs -> length xs == 0) (previousEdges current g)

-- Euleri tee leidmise abimeetod
eulerPath' :: (Graph a b g, Ord a) => [b] -> a -> g -> [b]
eulerPath' acc v g = maybe acc (\es -> eulerPath'' acc es v g) (nextEdges v g)

-- Euleri tsükli leidmine
eulerCycle :: (Graph a b g, Ord a) => g -> Maybe [b]
eulerCycle g
    | Prelude.null $ edges g = Nothing
    | not $ edgesCondition g = Nothing
    | not $ connected g = Nothing
    | otherwise = Just $ Prelude.reverse $ eulerPath' [] (head (vertices g)) g