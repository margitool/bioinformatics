import LabeledGraph
import Graph
import Data.List
import Data.Maybe

import Test.QuickCheck

instance (Arbitrary a, Arbitrary b)
        => Arbitrary (LabeledEdge a b) where
    arbitrary = do
        x <- arbitrary
        y <- arbitrary
        label <- arbitrary
        return (LabeledEdge x y label)

instance (Arbitrary a, Arbitrary b, Ord a, Ord b)
        => Arbitrary (LabeledGraph a b) where
    arbitrary = do
        xs <- arbitrary
        return (fromList xs)

type TestVertex = String
type TestEdge = LabeledEdge String String
type TestGraph = LabeledGraph String String

--graaf on iseendaga võrdne
propEquality :: TestGraph -> Bool
propEquality g = g == g

-- tühjal graafi servadeks on tühi list
propEmptyHasNoEdges :: Bool
propEmptyHasNoEdges = Prelude.null $ edges (empty :: TestGraph)

-- tühja graafi tippudeks on tühi list
propEmptyHasNoVertices :: Bool
propEmptyHasNoVertices = Prelude.null $ vertices (empty :: TestGraph)

-- graafi tipu lisamise tagajärel on see tipp graafi tippude hulgas olemas
propAddVertex :: TestVertex -> TestGraph -> Bool
propAddVertex v g = elem v $ vertices $ addVertex v g

-- graafi serva lisamise tagajärel on see serv graafi servade hulgas olemas
propAddEdge :: TestEdge -> TestGraph -> Bool
propAddEdge e g = elem e $ edges $ addEdge e g

-- graafi tippude lisamise järjekord ei ole oluline
propOrderOfAddingVertices :: TestVertex -> TestVertex -> TestGraph -> Bool
propOrderOfAddingVertices va vb g =
    (addVertex va $ addVertex vb g) == (addVertex vb $ addVertex va g)

-- graafi servade lisamise järjekord ei ole oluline
propOrderOfAddingEdges :: TestEdge -> TestEdge -> TestGraph -> Bool
propOrderOfAddingEdges ea eb g =
    (addEdge ea $ addEdge eb g) == (addEdge eb $ addEdge ea g)

-- graafi serva lisades lisatakse ka serva algus- ja lõpptipp
propAddingEdgeAddsEdgeVertices :: TestEdge -> TestGraph -> Bool
propAddingEdgeAddsEdgeVertices e g =
    elem (start e) newGraphVertices && elem (finish e) newGraphVertices
    where newGraphVertices =
            vertices $ addEdge e $ removeVertex (start e)
            $ removeVertex (finish e) g

-- tipu eemaldamise tagajärel ei ole see tipp olemas graafi tippude hulgas
propRemoveVertex :: TestVertex -> TestGraph -> Bool
propRemoveVertex v g = notElem v $ vertices $ removeVertex v g

-- serva eemaldamise tagajärel ei ole see serv olemas graafi servade hulgas
propRemoveEdge :: TestEdge -> TestGraph -> Bool
propRemoveEdge e g = notElem e $ edges $ removeEdge e g

-- tipu eemaldamise tagajärel ei ole see tipp olemas graafi tippude hulgas
propRemovingVerticeRemovesEdges :: TestEdge -> TestGraph -> Bool
propRemovingVerticeRemovesEdges e g =
    all (\e -> notEdge v e) $ edges $ removeVertex v newGraph
    where   v = start e
            notEdge v e = (start e) /= v && (finish e) /= v
            newGraph = addEdge e $ addEdge (Graph.reverse e) g

-- transponeeritud graafi tipu hulgas on samad elemendid,
-- kui esialgse graafi tippude hulgas
propTransposeHasSameVertices :: TestGraph -> Bool
propTransposeHasSameVertices g =
    sameElems (vertices g) (vertices $ Graph.transpose g)
    where sameElems xs ys = xs \\ ys == [] && ys \\ xs == []

-- kui transponeerida transponeeriutd graaf, saadakse esialgsega võrdne graaf
propTransposingTwice :: TestGraph -> Bool
propTransposingTwice g = g == (Graph.transpose $ Graph.transpose g)

-- serva ümber pööramise kontroll
propReverseEdge :: TestEdge -> Bool
propReverseEdge e = isReverse e (Graph.reverse e)
    where isReverse (LabeledEdge xa ya la) (LabeledEdge xb yb lb) =
            xa == yb && xb == ya && la == lb

-- transponeeritud graafis on kõik servad esialgse graafi vastasservad
propTransposeHasOppositeEdges :: TestGraph -> Bool
propTransposeHasOppositeEdges g = hasReverseEdges g t && hasReverseEdges t g
    where   t = Graph.transpose g
            hasReverseEdges ga gb =
                all (\e -> elem (Graph.reverse e) (edges gb)) (edges ga)

-- serva kahekordne ümberpööramine annab esialgse servaga võrdse serva
propReversingTwice :: TestEdge -> Bool
propReversingTwice e = e == (Graph.reverse $ Graph.reverse e)

--abimeetod
elemInMaybe :: (Eq a) => a -> Maybe [a] -> Bool
elemInMaybe e m = maybe False (elem e) m

-- serv on olemas selle algustipust väljuvate servade hulgas
propNextEdges :: TestEdge -> TestGraph -> Bool
propNextEdges e g = elemInMaybe e $ nextEdges (start e) $ addEdge e g

-- serv on olemas selle lõpptippu suubuvate servade hulgas
propPreviousEdges :: TestEdge -> TestGraph -> Bool
propPreviousEdges e g = elemInMaybe e $ previousEdges (finish e) $ addEdge e g

--abimeetod
vertexNotInGraph :: TestVertex -> TestGraph -> Bool
vertexNotInGraph v g = notElem v $ vertices g

-- kui leida väljuvaid servi tipule, mida ei ole graafis,
-- on tulemuseks Nothing
propNextEdgesNothing :: TestVertex -> TestGraph -> Property
propNextEdgesNothing v g =
    (vertexNotInGraph v g) ==> isNothing $ nextEdges v g

-- kui leida suubuvaid servi tipule, mida ei ole graafis,
-- on tulemuseks Nothing
propPreviousEdgesNothing :: TestVertex -> TestGraph -> Property
propPreviousEdgesNothing v g =
    (vertexNotInGraph v g) ==> isNothing $ previousEdges v g
