{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}

-- tüübiklass graafi ja sellele vastava tippude ja servade esitamiseks
module Graph (
    Edge,
    start,
    finish,
    Graph.reverse,
    Graph,
    Graph.empty,
    addEdge,
    addVertex,
    removeEdge,
    removeVertex,
    nextEdges,
    previousEdges,
    vertices,
    edges,
    transpose,
    Graph.null
    ) where

-- serv e, mis vastab tipule a
class Edge e a | e -> a where
    start :: e -> a
    finish :: e -> a
    reverse :: e -> e

-- graaf g, mis vastab tipule a ja servale b
class (Edge b a) => Graph a b g | g -> b, g -> a where
    -- nullgraaf
    empty :: g
    -- tipu lisamine
    addVertex :: a -> g -> g
    -- serva lisamine
    addEdge :: b -> g -> g
    -- tipu eemaldamine
    removeVertex :: a -> g -> g
    -- serva eemaldamine    
    removeEdge :: b -> g -> g
    -- kõik antud tipust väljuvad kaared    
    nextEdges :: a -> g -> Maybe [b]
    -- kõik antud tippu suubuvad servad
    previousEdges :: a -> g -> Maybe [b]
    -- kõik graafi tipud
    vertices :: g -> [a]
    -- kõik graafi servad
    edges :: g -> [b]
    -- graafi transponeerimine
    transpose :: g -> g
    -- kontroll, kas graaf on nullgraaf
    null :: g -> Bool