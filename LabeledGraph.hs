{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module LabeledGraph (
    LabeledGraph,
    LabeledEdge (LabeledEdge),
    fromList
    ) where

import Graph

import qualified Data.Map.Strict as Map
import qualified Data.Set as Set

-- sildistatud graaf: graafi tipud ja servad on tähistatud
data LabeledGraph a b = LabeledGraph (Map.Map a (Set.Set (a,b))) deriving (Show, Eq)

-- antud graafile vastav serv
data LabeledEdge a b = LabeledEdge a a b deriving (Show, Eq)

instance Edge (LabeledEdge a b) a where
    start (LabeledEdge x _ _) = x
    finish (LabeledEdge _ x _) = x
    reverse (LabeledEdge x y l) = LabeledEdge y x l

-- graafi koostamine kolmikutest, mis esindavad serva: algustipp, lõpptipp ja serva tähis
fromList :: (Ord a, Ord b) => [(a, a, b)] -> LabeledGraph a b
fromList = foldl (\acc (x,y,z) -> addEdge (LabeledEdge x y z) acc) empty

instance (Ord a, Ord b) => Graph a (LabeledEdge a b) (LabeledGraph a b) where
    -- tühi graaf
    empty = LabeledGraph Map.empty

    -- serva lisamine. kui servale vastavaid tippe graafis ei ole, lisatakse ka tipud
    addEdge (LabeledEdge x y l) (LabeledGraph m) =
        addVertex y 
        $ LabeledGraph 
        $ Map.insertWith Set.union x (Set.singleton (y, l)) m

    -- tipu lisamine
    addVertex v (LabeledGraph m) =
        LabeledGraph $ Map.insertWith (\_ old -> old) v (Set.empty) m

    -- serva eemaldamine. Kui graafis serva ei ole, on tulemuseks esialgne graaf
    removeEdge (LabeledEdge x y l) (LabeledGraph m) =
        LabeledGraph $ Map.adjust (Set.delete (y,l)) x m
    
    -- tipu eemaldamine. Kui graafis tippu ei ole, on tulemuseks esialgne graaf
    removeVertex v (LabeledGraph m) =
        foldl (\acc e -> removeEdge e acc)
            (LabeledGraph $ Map.delete v m) $ previousEdges' v m

    -- tipust väljuvad servad. Kui graafis tippu ei ole, on tulemuseks Nothing
    nextEdges v (LabeledGraph m) =
        fmap (foldl (\acc (y,l) -> (LabeledEdge v y l):acc) []) $ Map.lookup v m
    
    -- tippu suubuvad servad. Kui graafis tippu ei ole, on tulemuseks Nothing
    previousEdges v (LabeledGraph m)
        | Map.notMember v m = Nothing
        | otherwise =
            Just $ previousEdges' v m
    
    -- graafi tipud
    vertices (LabeledGraph m) = Map.keys m

    -- graafi servad
    edges (LabeledGraph m) = Map.foldlWithKey (\acc x es -> foldl (\acc' (y,l) -> (LabeledEdge x y l):acc') acc es) [] m

    -- graafi transponeerimine
    transpose (LabeledGraph m) = Map.foldlWithKey (\acc x es -> foldl (\acc' (y,l) -> addEdge (LabeledEdge y x l) acc') acc es ) empty m

    -- nullgraafi kontroll
    null (LabeledGraph m) = Map.null m

previousEdges' v m = Map.foldlWithKey 
            (\acc x es -> foldl
                (\acc' (y,l) -> 
                    if y == v then (LabeledEdge x y l):acc' else acc')
                    acc es
                    )
            [] m