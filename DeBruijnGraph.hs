module DeBruijnGraph (
    Label,
    DeBruijnEdge,
    DeBruijnGraph,
    Alphabet,
    alphabet,
    allWords,
    graphFromEdges,
    getCompleteDeBruijnVertices,
    getDeBruijnSequence,
    getCompleteDeBruijnEdges
    ) where

import qualified Data.Set as Set
import LabeledGraph
import Graph
import Euler

-- klass tähestiku esitamiseks
class Alphabet a where
    alphabet :: Set.Set a

-- tüüp sõne või nimetuse esitamiseks: massiiv tähestiku tähtedest
type Label a = [a]

-- de Bruijni graaf ja de Bruijni kraafi serv
type DeBruijnEdge a = LabeledEdge (Label a) (Label a)
type DeBruijnGraph a = LabeledGraph (Label a) (Label a)

-- leiab kõikvõimalikud sõned pikkusega n
allWords :: (Alphabet a, Ord a) => Int -> [[a]]
allWords n
    | n <= 0 = [[]]
    | otherwise = [x:xs | x <- Set.toList alphabet, xs <- allWords (n-1)]

-- lisab serva graafi juhul, kui kehtib de Bruijni graafi serva tingimus
addDeBruijnEdge :: (Ord a) => Label a -> Label a -> DeBruijnGraph a -> DeBruijnGraph a
addDeBruijnEdge [] _ g = g
addDeBruijnEdge _ [] g = g
addDeBruijnEdge xs ys g
    | (tail xs) == (init ys) = addEdge (LabeledEdge xs ys ((head xs) : ys)) g
    | otherwise = g

-- servade lisamine
addEdges :: (Ord a) => Label a -> [Label a] -> DeBruijnGraph a -> DeBruijnGraph a
addEdges x ys g = Prelude.foldl (\acc y -> addDeBruijnEdge x y acc) g ys

-- koostab ilma servadeta graafi tippudest
verticeGraph :: (Ord a) => [Label a] -> DeBruijnGraph a
verticeGraph xs = Prelude.foldl (\acc x -> addVertex x acc) Graph.empty xs

-- de Bruijn graafi koostamine, kui sisendiks on tipud
getDeBruijnGraphVertices :: (Ord a) => [Label a] -> DeBruijnGraph a
getDeBruijnGraphVertices xs =
    Prelude.foldl (\acc x -> addEdges x xs acc)
    (verticeGraph xs)
    xs

-- de Bruijn graaf, kus tippudeks on kõikvõimalikud sõned pikkusega n
getCompleteDeBruijnVertices :: (Ord a, Alphabet a) => Int -> DeBruijnGraph a
getCompleteDeBruijnVertices n = getDeBruijnGraphVertices $ allWords n

-- de Bruijni järjestiku leidmise abimeetod
getDeBruijnSequence' :: [DeBruijnEdge a] -> Label a
getDeBruijnSequence' [] = []
getDeBruijnSequence' ((LabeledEdge _ _ l):xs) =
    (head l) : (getDeBruijnSequence' xs)

-- deBruijn järjestus de Bruijn graafist
getDeBruijnSequence :: (Ord a) => DeBruijnGraph a -> Maybe (Label a)
getDeBruijnSequence g = fmap (getDeBruijnSequence') $ eulerCycle g

-- de Bruijni graafi serve loomine serva nimetuse järgi
getEdge :: Label a -> DeBruijnEdge a
getEdge xs = LabeledEdge (init xs) (tail xs) xs

-- de Bruijn graafi koostamine, kui sisendiks on servad
graphFromEdges :: (Ord a) => [Label a] -> DeBruijnGraph a
graphFromEdges = foldl (\acc x -> addEdge (getEdge x) acc) Graph.empty

--de Bruijn graaf, kus servadeks on kõikvõimalikud sõned pikkusega n
getCompleteDeBruijnEdges :: (Ord a, Alphabet a) => Int -> DeBruijnGraph a
getCompleteDeBruijnEdges n = graphFromEdges $ allWords n
