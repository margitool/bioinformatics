import Test.HUnit

import LabeledGraph
import Euler
import Graph
import DeBruijnGraph
import BioInformatics

import Data.List
import Data.Maybe
import qualified Data.Set as Set

data Bin = Zero | One deriving (Eq, Ord)

instance Show Bin where
    show Zero = "0"
    show One = "1"

instance Alphabet Bin where
    alphabet = Set.fromList [Zero, One]

-- alfabeedi kõikide sõnade puhul Euleri tsükli põhjal leitav järjestus
-- alati olemas nii tippudest kui servadest koostatud graafil
-- juhul kui saadud graafis puudub Euleri tsykkel, on tulemuseks Nothing
existsFromVertices :: Int -> Bool
existsFromVertices n = isJust $ getDeBruijnSequence $ graph
    where graph = getCompleteDeBruijnVertices n :: DeBruijnGraph Bin

message1 = "Got Nothing but Expected Maybe"

testExists1 = TestCase (assertBool message1 $ existsFromVertices 5)
testExists2 = TestCase (assertBool message1 $ existsFromVertices 3)

existsFromEdges :: Int -> Bool
existsFromEdges n = isJust $ getDeBruijnSequence $ graph
    where graph = getCompleteDeBruijnEdges n :: DeBruijnGraph Bin

testExists3 = TestCase (assertBool message1 $ existsFromEdges 6)
testExists4 = TestCase (assertBool message1 $ existsFromEdges 4)

testExists = TestList [testExists1, testExists2, testExists3, testExists4]

-- DeBruijn järjestuses kõikvõimalikud kombinatsioonid olemas
allWordsPresentBin :: Int -> Bool
allWordsPresentBin n = all (\w -> isInfixOf w (binSequence ++ binSequence)) 
                        $ allWords n
    where binSequence = fromJust 
                        $ getDeBruijnSequence
                        $ getCompleteDeBruijnEdges n :: Label Bin

allWordsPresentDna :: Int -> Bool
allWordsPresentDna n = all (\w -> isInfixOf w (dnaSequence ++ dnaSequence)) 
                        $ allWords n
    where dnaSequence = fromJust 
                        $ getDeBruijnSequence
                        $ getCompleteDeBruijnEdges n :: Dna

message2 = "All words not present"

testAllWordsBin1 = TestCase (
    assertBool message2 $ allWordsPresentBin 7
    )
testAllWordsBin2 = TestCase (
    assertBool message2 $ allWordsPresentBin 6
    )
testAllWordsDna = TestCase (
    assertBool message2 $ allWordsPresentDna 3
    )

testAllWordsPresent = TestList [
    testAllWordsBin1,
    testAllWordsBin2,
    testAllWordsDna
    ]

-- de Bruijn järjestuse pikkus kõikidest variantidest on k ^ n
deBruijnSequenceLengthBin :: Int -> Int
deBruijnSequenceLengthBin n = length binSequence
    where binSequence = fromJust 
                        $ getDeBruijnSequence 
                        $ getCompleteDeBruijnEdges n :: Label Bin

deBruijnSequenceLengthDna :: Int -> Int
deBruijnSequenceLengthDna n = length dnaSequence
    where dnaSequence = fromJust 
                        $ getDeBruijnSequence 
                        $ getCompleteDeBruijnEdges n :: Dna

message3 = "Lenght not equal to k ^ n";

testBinLength1 = TestCase (
    assertEqual message3 (2 ^ 5) (deBruijnSequenceLengthBin 5)
    )

testBinLength2 = TestCase (
    assertEqual message3 (2 ^ 7) (deBruijnSequenceLengthBin 7)
    )

testDnaLength = TestCase(
    assertEqual message3 (4 ^ 5) (deBruijnSequenceLengthDna 5)
    )

testLength = TestList [testBinLength1, testBinLength2, testDnaLength]

binSequence :: Int -> Label Bin
binSequence n = fromJust $ getDeBruijnSequence $ getCompleteDeBruijnEdges n

-- ette antud väärtustega saab õige
-- otsitav väärtus on tulemuses olemas
-- saadud tulemuse pikkus on sisendis olevate sõnede arv

values = ["abc", "bcd", "cda", "dab"]
expectedValue = "abcd"
expectedLength = Just 4

maybeContains :: String -> Maybe String -> Bool
maybeContains _ Nothing = False
maybeContains x (Just y) = isInfixOf x (y ++ y)

result = getDeBruijnSequence $ graphFromEdges values
resultLength = fmap length result

testWithValuesExist = TestCase (
    assertBool (expectedValue ++ " not in " ++ (show result))
    (maybeContains expectedValue result)
    )

testWithValuesLength = TestCase (
    assertEqual "Either no result or result with wrong length" expectedLength resultLength
    )

testWithValues = TestList [
    TestLabel "Expected value in result" testWithValuesExist,
    TestLabel "Expected result is the legth of strings in input" testWithValuesLength
    ]

-- kui ei ole täielikult kaetud, saab vastuseks Nothing

values2 = ["abc", "bcd", "cda"]
result2 = getDeBruijnSequence $ graphFromEdges values2

testNothing = TestCase (
    assertBool ("Expected Nothing but got " ++ (show result2)) (isNothing result2)
    )

-- DeBruijn graph kõikidest variantidest kasutades n-pikkusega tippi on sama, 
-- mis DeBruijn graph kõikidest variantidest kasutades (n+1)-pikkusega servi

deBruijnV1 :: DeBruijnGraph Bin
deBruijnV1 = getCompleteDeBruijnVertices 4

deBruijnE1 :: DeBruijnGraph Bin
deBruijnE1 = getCompleteDeBruijnEdges (4+1)

deBruijnV2 :: DeBruijnGraph Bin
deBruijnV2 = getCompleteDeBruijnVertices 6

deBruijnE2 :: DeBruijnGraph Bin
deBruijnE2 = getCompleteDeBruijnEdges (6+1)

testEdgesAndVertices1 = TestCase(
    assertEqual "Edges of 4 vs vertices of 5" deBruijnV1 deBruijnE1
    )

testEdgesAndVertices2 = TestCase(
    assertEqual "Edges of 6 vs vertices of 7" deBruijnV2 deBruijnE2
    )

testEdgesAndVertices = TestList [testEdgesAndVertices1, testEdgesAndVertices2]

allTests = TestList [
    TestLabel "Exists sequence based on Euler cycle" testExists,
    TestLabel "All words present in de Bruijn sequence" testAllWordsPresent,
    TestLabel "De Bruijn sequence length is k ^ n" testLength,
    TestLabel "Vertices and edges" testEdgesAndVertices,
    TestLabel "Etteantud väärtustega" testWithValues,
    TestLabel "Kui Euleri tsükkel puudub" testNothing
    ]