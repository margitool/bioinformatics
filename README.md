# README #

## Implementing de Bruijn Sequence and de Bruijn Graph with Haskell ##

### What is this repository for? ###

This repository holds an example of implementing the de Bruijn sequence and de Bruijn Graph with Haskell. The goal of this is to develop a bioinformatics tool that can be used for genome assembly.

This project started as a thesis project for Estonian IT College.

### How do I get set up? ###

Either clone or download the code from source.

For TestingDeBruijnGraph.hs and TestingGraph.hs QuickCheck is used.

For TestingDeBruijnSequence.hs HUnit was used.

These tools have to be installed using cabal. See the documentations for details:

HUNit - https://wiki.haskell.org/HUnit_1.0_User%27s_Guide

QuickCheck - https://hackage.haskell.org/package/QuickCheck

To get Haskell go to https://www.haskell.org/

Details about Cabal can be found here: https://www.haskell.org/cabal/


### Who do I talk to? ###

The owner of this repository is Margit Scheler (previously named Margit Ool). Any questions? E-mail me margit.ool@hotmail.com