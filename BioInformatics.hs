{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeSynonymInstances #-}

module BioInformatics (
    Nucleotide,
    Dna
    )
    where

import DeBruijnGraph
import qualified Data.Set as Set


-- näide andmetüübist, mille abil saaks esitada nukleotiide
data Nucleotide = Adenine | Cytosine | Guanine | Thymine deriving (Eq, Ord)

type Dna = Label Nucleotide

instance Show Nucleotide where
    show Adenine = "A"
    show Cytosine = "C"
    show Guanine = "G"
    show Thymine = "T"

instance Alphabet Nucleotide where
    alphabet = Set.fromList [Adenine, Cytosine, Guanine, Thymine]